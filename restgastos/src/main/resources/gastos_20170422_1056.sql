-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.54-community


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema gastos
--

CREATE DATABASE IF NOT EXISTS gastos;
USE gastos;

--
-- Definition of table `conceptos`
--

DROP TABLE IF EXISTS `conceptos`;
CREATE TABLE `conceptos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) DEFAULT NULL,
  `observacion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conceptos`
--

/*!40000 ALTER TABLE `conceptos` DISABLE KEYS */;
INSERT INTO `conceptos` (`id`,`descripcion`,`observacion`) VALUES 
 (1,'Compras','Compras'),
 (2,'Compras Hogar','Compras para el hogar, por ejemplo cortinas.'),
 (3,'Obra Social','Obra Social para atención médico'),
 (4,'Jubilación','Pago del haber jubilatorio'),
 (6,'Cable e Internet','Servicio de Cable TV e Internet...'),
 (7,'Cenas','Cenas en restaurantes y bares');
/*!40000 ALTER TABLE `conceptos` ENABLE KEYS */;


--
-- Definition of table `gastos`
--

DROP TABLE IF EXISTS `gastos`;
CREATE TABLE `gastos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `importe` double NOT NULL,
  `id_concepto` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKB55017DF48482166` (`id_concepto`),
  CONSTRAINT `FKB55017DF48482166` FOREIGN KEY (`id_concepto`) REFERENCES `conceptos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gastos`
--

/*!40000 ALTER TABLE `gastos` DISABLE KEYS */;
INSERT INTO `gastos` (`id`,`fecha`,`importe`,`id_concepto`) VALUES 
 (2,'2014-01-09 00:11:00',111,1),
 (3,'2015-01-30 00:01:00',100,1),
 (4,'2015-01-30 00:01:00',112.66,1),
 (5,'2015-01-29 21:00:00',200,1),
 (6,'2015-01-29 21:00:00',333,1),
 (7,'2015-01-29 21:00:00',200,1),
 (8,'2015-01-29 21:00:00',200,1);
/*!40000 ALTER TABLE `gastos` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
