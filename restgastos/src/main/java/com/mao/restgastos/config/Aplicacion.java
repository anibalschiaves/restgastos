package com.mao.restgastos.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.mao.restgastos.config","com.mao.restgastos.controller"})
public class Aplicacion {
	
	public static void main(String[] args) {
        SpringApplication.run(Aplicacion.class, args);
    }
}
