package com.mao.restgastos.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mao.restgastos.dao.ConceptoDao;
import com.mao.restgastos.dao.GastoDao;

@Configuration
@EnableTransactionManagement
public class PersistenceJPAConfig {
	
	@Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource());
		em.setPackagesToScan(new String[] { "com.mao.restgastos.model","com.mao.restgastos.dao" });
 
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(additionalProperties());
 
		return em;
	}
	
	@Bean
	public DataSource dataSource(){
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/gastos");
		dataSource.setUsername( "root" );
		dataSource.setPassword( "root" );
		return dataSource;
	}
	
	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf){
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);
 
		return transactionManager;
	}
	
	 @Bean
	 public PersistenceExceptionTranslationPostProcessor exceptionTranslation(){
		 return new PersistenceExceptionTranslationPostProcessor();
	 }
 
	 Properties additionalProperties() {
		 Properties properties = new Properties();
		 properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
		 properties.setProperty("hibernate.show_sql", "true");
		 return properties;
	 }
	 
	 //Beans for Repositories, replace for autoscaning mode
	 @Bean
	 public ConceptoDao getConceptoDato() {
		 return new ConceptoDao();
	 }
	 
	 @Bean
	 public GastoDao getGastoDato() {
		 return new GastoDao();
	 }
}
