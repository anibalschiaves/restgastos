package com.mao.restgastos.controller;

import java.util.ArrayList;
import java.util.List;

public class ValidacionErrores {
	
	private boolean tieneError;
	private List<String> mensaje;
	
	public boolean isTieneError() {
		return tieneError;
	}
	public void setTieneError(boolean tieneError) {
		this.tieneError = tieneError;
	}
	public List<String> getMensaje() {
		return mensaje;
	}
	public void setMensaje(List<String> mensaje) {
		this.mensaje = mensaje;
	}
	
	public void addMensaje(String mensaje) {
		if (this.mensaje==null) this.mensaje = new ArrayList<String>();
		this.mensaje.add(mensaje);
	}

}
