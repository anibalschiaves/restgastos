package com.mao.restgastos.controller;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mao.restgastos.dao.ConceptoDao;
import com.mao.restgastos.model.Concepto;

@RestController
@RequestMapping("/concepto")
public class ConceptoController {
	
	@Autowired
	private ConceptoDao conceptoDao;
	
	@RequestMapping(value="/all",method=RequestMethod.GET)
	public List<Concepto> getAll() {
		return conceptoDao.findAll();
	}
	
	@RequestMapping(value="/get/{id}",method=RequestMethod.GET)
	public Concepto getById(@PathVariable Long id) {
		return conceptoDao.findById(id, false);
	}
	
	@RequestMapping(value="/post",method=RequestMethod.POST)
	public ValidacionErrores post(@RequestBody Concepto c) {
		ValidacionErrores err = new ValidacionErrores();
		try {
			if (c.getDescripcion()==null || c.getDescripcion().isEmpty()) {
				err.setTieneError(true);
				err.addMensaje("Debe ingresar una descripcion");
			} else {
				c.setId(null);//Seteo en nulo porque es un insert
				this.conceptoDao.saveOrUpdate(c);
				err.setTieneError(false);
				err.addMensaje("Se guardo correctamente con id "+c.getId());
			}
		} catch (SQLException e) {
			err.setTieneError(true);
			err.addMensaje(e.getMessage());
		}
		return err;
	}
	
	@RequestMapping(value="/put/{id}",method=RequestMethod.PUT)
	public ValidacionErrores put(@PathVariable Long id, @RequestBody Concepto c) {
		ValidacionErrores err = new ValidacionErrores();
		try {
			Concepto o = this.conceptoDao.findById(id, false);
			o.doEqual(c);
			this.conceptoDao.saveOrUpdate(o);
			err.setTieneError(false);
			err.addMensaje("Se envió correctamente");
		} catch (Exception e) {
			err.setTieneError(true);
			err.addMensaje(e.getMessage());
		}
		return err;
	}
	
	@RequestMapping(value="/delete/{id}",method=RequestMethod.DELETE)
	public ValidacionErrores delete(@PathVariable Long id) {
		ValidacionErrores err = new ValidacionErrores();
		try {
			Concepto c = this.conceptoDao.findById(id, false);
			this.conceptoDao.makeTransient(c);
			err.setTieneError(false);
			err.addMensaje("Se envió correctamente");
		} catch (Exception e) {
			err.setTieneError(true);
			err.addMensaje(e.getMessage());
		}
		return err;
	}

}
