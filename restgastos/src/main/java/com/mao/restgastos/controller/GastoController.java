package com.mao.restgastos.controller;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mao.restgastos.dao.GastoDao;
import com.mao.restgastos.model.Concepto;
import com.mao.restgastos.model.Gasto;

@RestController
@RequestMapping("/gasto")
public class GastoController {
	
	@Autowired
	private GastoDao gastoDao;
	
	@RequestMapping(value="/all",method=RequestMethod.GET)
	public List<Gasto> getAll() {
		return this.gastoDao.findAll();
	}
	
	@RequestMapping(value="/get/{id}",method=RequestMethod.GET)
	public Gasto getById(@PathVariable Long id) {
		Gasto g = this.gastoDao.findById(id, false);
		g.getConcepto().getDescripcion();
		return g;
	}
	
	@RequestMapping(value="/post",method=RequestMethod.POST)
	public ValidacionErrores post(@RequestBody Gasto g) {
		ValidacionErrores err = new ValidacionErrores();
		try {
			boolean tieneErr = false;
			if (g.getConcepto()==null) {
				err.setTieneError(true);
				err.addMensaje("Debe ingresar un concepto");
				tieneErr = true;
			} 
			if (g.getFecha()==null) {
				err.setTieneError(true);
				err.addMensaje("Debe ingresar una fecha");
				tieneErr = true;
			}
			if (g.getImporte()==null) {
				err.setTieneError(true);
				err.addMensaje("Debe ingresar un importe");
				tieneErr = true;
			}
			if (!tieneErr) {
				g.setId(null);//Seteo en nulo porque es un insert
				g = this.gastoDao.saveOrUpdate(g);
				err.setTieneError(false);
				err.addMensaje("Se guardo correctamente con id "+g.getId());
			}
		} catch (SQLException e) {
			err.setTieneError(true);
			err.addMensaje(e.getMessage());
		}
		return err;
	}
	
	@RequestMapping(value="/put/{id}",method=RequestMethod.PUT)
	public ValidacionErrores put(@PathVariable Long id, @RequestBody Gasto c) {
		ValidacionErrores err = new ValidacionErrores();
		try {
			Gasto g = this.gastoDao.findById(id, false);
			g.doEqual(c);
			this.gastoDao.saveOrUpdate(g);
			err.setTieneError(false);
			err.addMensaje("Se envió correctamente");
		} catch (Exception e) {
			err.setTieneError(true);
			err.addMensaje(e.getMessage());
		}
		return err;
	}
	
	@RequestMapping(value="/delete/{id}",method=RequestMethod.DELETE)
	public ValidacionErrores delete(@PathVariable Long id) {
		ValidacionErrores err = new ValidacionErrores();
		try {
			Gasto g = this.gastoDao.findById(id, false);
			this.gastoDao.makeTransient(g);
			err.setTieneError(false);
			err.addMensaje("Se envió correctamente");
		} catch (Exception e) {
			err.setTieneError(true);
			err.addMensaje(e.getMessage());
		}
		return err;
	}

}
