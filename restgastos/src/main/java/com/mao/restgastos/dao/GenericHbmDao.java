/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mao.restgastos.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;



/**
 *
 * @author Ivan
 */
public abstract class GenericHbmDao<T, ID extends Serializable> {

	@PersistenceContext
    private EntityManager entityManager;

    protected Class<T> persistentClass;
    private Session session;
    
    public GenericHbmDao() {
    	Type type = getClass().getGenericSuperclass();
        ParameterizedType paramType = (ParameterizedType) type;
        this.persistentClass = (Class<T>) paramType.getActualTypeArguments()[0];
    }
    
    public void setPersistentClass(Class<T> persistentClass) {
		this.persistentClass = persistentClass;
	}

	//@SuppressWarnings("unchecked")
    public final void setSession(Session s) {
        this.session = s;
    }

    public Session getSession() {
//        if (session == null) {
//            throw new IllegalStateException("Session has not been set on DAO before usage");
//        }
        return (Session) entityManager.unwrap(Session.class);
    }

    public Class<T> getPersistentClass() {
        return persistentClass;
    }

    public T findById(ID id, boolean lock) {
        T entity;
        if (lock) {
            entity = (T) getSession().load(getPersistentClass(), id, LockMode.UPGRADE);
        } else {
            entity = (T) getSession().load(getPersistentClass(), id);
        }
        return entity;
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll() {
        return findByCriteria();
    }

    @SuppressWarnings("unchecked")
    public List<T> findByExample(T exampleInstance, String[] excludeProperty) {
        Criteria crit = getSession().createCriteria(getPersistentClass());
        Example example = Example.create(exampleInstance);
        for (String exclude : excludeProperty) {
            example.excludeProperty(exclude);
        }
        crit.add(example);
        return crit.list();
    }

    @SuppressWarnings("unchecked")
    public T makePersistent(T entity) {
    	getSession().saveOrUpdate(entity);
    	return entity;
    }

    public void makeTransient(T entity) {
        getSession().delete(entity);
        this.flush();
    }

    public void flush() {
        getSession().flush();
    }

    public void clear() {
        getSession().clear();
    }

    /**
     * Use this inside subclasses as a convenience method.
     */
    //@SuppressWarnings("unchecked")
    protected List<T> findByCriteria(Criterion... criterion) {
    	System.out.println(getSession());
    	System.out.println(getPersistentClass());
        Criteria crit = getSession().createCriteria(getPersistentClass());
        for (Criterion c : criterion) {
            crit.add(c);
        }
        return crit.list();
    }

    /**
     * Use this inside subclasses as a convenience method.
     */
    //@SuppressWarnings("unchecked")
    protected List<T> findByCriteria(List<Criterion> criterion) {
        Criteria crit = getSession().createCriteria(getPersistentClass());
        for (Criterion c : criterion) {
            crit.add(c);
        }
        return crit.list();
    }

    /**
     * Ordered list
     *
     * @param criterion
     * @param order
     * @return
     */
    //@SuppressWarnings("unchecked")
    protected List<T> findByCriteria(List<Order> order, Criterion... criterion) {
    	Criteria crit = getSession().createCriteria(getPersistentClass());
        for (Criterion c : criterion) {
            crit.add(c);
        }
        for (Order o : order) {
            crit.addOrder(o);
        }
        return crit.list();
    }
    
    /**
     * Ordered list
     *
     * @param criterion
     * @param order
     * @return
     */
    //@SuppressWarnings("unchecked")
    protected List<T> findByCriteria(List<Order> order, Integer maxResult, Criterion... criterion) {
        Criteria crit = getSession().createCriteria(getPersistentClass());
        for (Criterion c : criterion) {
            crit.add(c);
        }
        for (Order o : order) {
            crit.addOrder(o);
        }
        crit.setMaxResults(maxResult);
        return crit.list();
    }
    
    /*
    public List<T> findAllActive(Suscriptor sus, List<Long> idsAdd) {
        Criteria crit = getSession().createCriteria(getPersistentClass());
        Metamodel m = this.entityManager.getMetamodel();
        EntityType<T> t_ = m.entity(getPersistentClass());
        Criterion or = Restrictions.eq(t_.getAttribute("activo").getName(), true);
        for (Long id : idsAdd) {
            or = Restrictions.or(or, Restrictions.eq(t_.getAttribute("id").getName(), id));
        }
        if (sus!=null) {
            crit.add(Restrictions.eq(t_.getAttribute("suscriptor").getName(), sus));
        }
        crit.add(Restrictions.eq(t_.getAttribute("borrado").getName(), false));
        crit.add(or);
        return crit.list();
    }
    
    public List<T> findAllActive(Suscriptor sus, List<Long> idsAdd, List<String> attOrder) {
        Criteria crit = getSession().createCriteria(getPersistentClass());
        Metamodel m = this.entityManager.getMetamodel();
        EntityType<T> t_ = m.entity(getPersistentClass());
        if (attOrder!=null) {
        	for (String order : attOrder) {
        		crit.addOrder(Order.asc(t_.getAttribute(order).getName()).ignoreCase());
        	}
        }
        Criterion or = Restrictions.eq(t_.getAttribute("activo").getName(), true);
        for (Long id : idsAdd) {
            or = Restrictions.or(or, Restrictions.eq(t_.getAttribute("id").getName(), id));
        }
        if (sus!=null) {
            crit.add(Restrictions.eq(t_.getAttribute("suscriptor").getName(), sus));
        }
        crit.add(Restrictions.eq(t_.getAttribute("borrado").getName(), false));
        crit.add(or);
        return crit.list();
    }
    */
    
    public boolean getBorrado(Long id) {
    	Criteria crit = getSession().createCriteria(getPersistentClass());
        Metamodel m = this.entityManager.getMetamodel();
        EntityType<T> t_ = m.entity(getPersistentClass());
        crit.add(Restrictions.eq(t_.getAttribute("id").getName(), id));
        crit.add(Restrictions.eq(t_.getAttribute("borrado").getName(), true));
        List<T> list = crit.list();
        return list==null?false:list.isEmpty()?false:true;
    }

    /**
     * Ordered list
     *
     * @param criterion
     * @param order
     * @return
     */
    //@SuppressWarnings("unchecked")
    public List<T> findByCriteria(List<Order> order, List<Criterion> criterion) {
    	Criteria crit = getSession().createCriteria(getPersistentClass());
        for (Criterion c : criterion) {
            crit.add(c);
        }
        for (Order o : order) {
            crit.addOrder(o);
        }
        return crit.list();
    }

    protected List<T> getAllOrdered(List<Order> order) {
        return findByCriteria(order, new ArrayList<Criterion>());
    }

    public T saveOrUpdate(T object) throws SQLException {
        object = this.makePersistent(object);
        this.flush();
        return object;
    }
    
    public T[] saveOrUpdate(T[] object) throws SQLException {
    	for (T o : object) {
            o = this.makePersistent(o);
        }
        this.flush();
        return object;
    }

    public void refresh(T entity) {
        getSession().refresh(entity);
    }
    
    public T revert(Long id, boolean detached){
    	getSession().evict((T)getSession().get(this.getPersistentClass(), id));
        T o = (T)getSession().get(this.getPersistentClass(), id);
        if (detached) {
        	getSession().evict(o);
        }
        return o;
    }
}
