package com.mao.restgastos.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mao.restgastos.model.Concepto;

@Repository
@Transactional
public class ConceptoDao extends GenericHbmDao<Concepto, Long>{
	
	
}
