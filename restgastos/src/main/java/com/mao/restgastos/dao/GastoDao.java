package com.mao.restgastos.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mao.restgastos.model.Gasto;

@Repository
@Transactional
public class GastoDao extends GenericHbmDao<Gasto, Long>{
	
	
}
