package com.mao.restgastos.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;

@Entity
@Table(name="gastos")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Gasto {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@JoinColumn(name="id_concepto")
	@ManyToOne(fetch=FetchType.EAGER)
	private Concepto concepto;
	
	@Column(name="importe")
	private BigDecimal importe;
	
	@Column(name="fecha")
	//@JsonSerialize(using = DateSerializerCustom.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private Date fecha;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Concepto getConcepto() {
		return concepto;
	}
	
	public void setConcepto(Concepto concepto) {
		this.concepto = concepto;
	}
	
	public BigDecimal getImporte() {
		return importe;
	}
	
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
	
	public Date getFecha() {
		return fecha;
	}
	
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public void doEqual(Gasto c) {
		this.setConcepto(c.getConcepto());
		this.setFecha(c.getFecha());
		this.setImporte(c.getImporte());
	}
	

}
