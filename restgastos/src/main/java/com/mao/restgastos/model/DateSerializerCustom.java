package com.mao.restgastos.model;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;

public class DateSerializerCustom extends DateSerializer{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

	@Override
	public void serialize(Date date, JsonGenerator gen, SerializerProvider provider)
	        throws IOException, JsonProcessingException {

	    String formattedDate = dateFormat.format(date);

	    gen.writeString(formattedDate);
	    
	}
}
